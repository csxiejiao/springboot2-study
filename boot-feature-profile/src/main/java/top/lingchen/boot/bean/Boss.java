package top.lingchen.boot.bean;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @Author 凌宸
 * @create 2021-12-12 下午 4:28
 * @Description
 * @Version 1.0
 */
@Data
@Component
@ConfigurationProperties("person")
@Profile(value = {"prod","default"})
@ToString
public class Boss implements Person{
    private String name;
    private Integer age;
}

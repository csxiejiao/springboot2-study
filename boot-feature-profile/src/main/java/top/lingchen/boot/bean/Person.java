package top.lingchen.boot.bean;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author 凌宸
 * @create 2021-12-12 下午 4:23
 * @Description
 * @Version 1.0
 */
public interface Person {
    String getName();
    Integer getAge();
    String toString();
}

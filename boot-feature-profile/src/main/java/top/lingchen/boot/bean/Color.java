package top.lingchen.boot.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author 凌宸
 * @create 2021-12-12 下午 4:33
 * @Description
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Color {
    private String color;
}

package top.lingchen.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import top.lingchen.boot.bean.Person;

/**
 * @Author 凌宸
 * @create 2021-12-12 下午 4:05
 * @Description
 * @Version 1.0
 */
@RestController
public class HelloController {
    @Value("${person.name:李四}")
    private String name;

    @Autowired
    private Person person;

    @Value("${JAVA_HOME}")
    private String msg;

    @Value("${os.name}")
    private String osName;

//    @GetMapping("/")
//    public String hello(){
//        return "Hello " + name;
//    }

    @GetMapping("/")
    public String hello(){
        return person.toString();
    }


    @GetMapping("/msg")
    public String getMsg(){
        return msg;
    }

    @GetMapping("/os")
    public String getOsName(){
        return osName;
    }

}

package top.lingchen.boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import top.lingchen.boot.bean.Color;

/**
 * @Author 凌宸
 * @create 2021-12-12 下午 4:33
 * @Description
 * @Version 1.0
 */
@Configuration
public class MyConfig {

    @Profile("prod")
    @Bean
    public Color red(){
        return new Color("red");
    }

    @Profile("test")
    @Bean
    public Color green(){
        return new Color("green");
    }
}

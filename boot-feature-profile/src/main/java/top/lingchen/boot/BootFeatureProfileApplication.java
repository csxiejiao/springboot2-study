package top.lingchen.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootFeatureProfileApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootFeatureProfileApplication.class, args);
    }

}

package top.lingchen.bootwebadmin.service;

import top.lingchen.bootwebadmin.bean.City;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 2:53
 * @Description
 * @Version 1.0
 */
public interface CityService {
    City getCityById(Integer id);

    void saveCity(City city);
}

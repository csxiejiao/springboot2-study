package top.lingchen.bootwebadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.lingchen.bootwebadmin.bean.User;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 2:55
 * @Description
 * @Version 1.0
 */
public interface UserService extends IService<User> {
}

package top.lingchen.bootwebadmin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.lingchen.bootwebadmin.bean.Account;
import top.lingchen.bootwebadmin.mapper.AccountMapper;
import top.lingchen.bootwebadmin.service.AccountService;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 10:01
 * @Description
 * @Version 1.0
 */
@Service
public class AccountServiceImp implements AccountService {

    @Autowired
    AccountMapper accountMapper;


    public Account getAccountById(Integer id){
        return accountMapper.getAccountById(id);
    }
}

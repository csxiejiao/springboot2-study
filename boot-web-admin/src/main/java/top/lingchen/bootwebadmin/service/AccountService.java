package top.lingchen.bootwebadmin.service;

import top.lingchen.bootwebadmin.bean.Account;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 2:52
 * @Description
 * @Version 1.0
 */
public interface AccountService {
    Account getAccountById(Integer id);
}

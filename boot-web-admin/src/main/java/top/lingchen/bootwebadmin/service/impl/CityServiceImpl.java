package top.lingchen.bootwebadmin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.lingchen.bootwebadmin.bean.City;
import top.lingchen.bootwebadmin.mapper.CityMapper;
import top.lingchen.bootwebadmin.service.CityService;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 10:29
 * @Description
 * @Version 1.0
 */
@Service
public class CityServiceImpl implements CityService {
    @Autowired
    CityMapper cityMapper;

    public City getCityById(Integer id){
        return cityMapper.getCityById(id);
    }

    public void saveCity(City city){
        cityMapper.insert(city);
    }

}

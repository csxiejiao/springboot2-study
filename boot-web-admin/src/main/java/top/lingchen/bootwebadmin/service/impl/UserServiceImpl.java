package top.lingchen.bootwebadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.lingchen.bootwebadmin.bean.User;
import top.lingchen.bootwebadmin.mapper.UserMapper;
import top.lingchen.bootwebadmin.service.UserService;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 2:51
 * @Description
 * @Version 1.0
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}

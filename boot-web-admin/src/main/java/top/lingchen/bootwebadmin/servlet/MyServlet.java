package top.lingchen.bootwebadmin.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 2:04
 * @Description
 * @Version 1.0
 */
//@WebServlet(urlPatterns = "/my") // 直接响应，没有经过 Spring 的拦截器
public class MyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write("hello world!");
    }
}

package top.lingchen.bootwebadmin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.lingchen.bootwebadmin.bean.Account;
import top.lingchen.bootwebadmin.service.AccountService;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 10:00
 * @Description
 * @Version 1.0
 */
@Controller
public class AccountController {

    @Autowired
    AccountService accountService;

    @ResponseBody
    @GetMapping("/acct")
    public Account getAccountById(@RequestParam("id") Integer id){
        return accountService.getAccountById(id);
    }
}

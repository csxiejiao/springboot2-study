package top.lingchen.bootwebadmin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 6:38
 * @Description
 * @Version 1.0
 */
@Controller
public class DataController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @ResponseBody
    @GetMapping("/sql")
    public String getCount(){
        Long count = jdbcTemplate.queryForObject("select count(*) from account_tbl", Long.class);
        return count.toString();
    }
}

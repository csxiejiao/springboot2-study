package top.lingchen.bootwebadmin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import top.lingchen.bootwebadmin.bean.User;
import top.lingchen.bootwebadmin.service.UserService;

import java.util.Arrays;
import java.util.List;

/**
 * @Author 凌宸
 * @create 2021-12-07 下午 6:04
 * @Description
 * @Version 1.0
 */
@Controller
public class TableController {

    @Autowired
    UserService userService;

    @GetMapping("/basic_table")
    public String basicTable(){

        return "tables/basic_table";
    }

    @GetMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id,
                             @RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             RedirectAttributes redirectAttributes){
        userService.removeById(id);
        redirectAttributes.addAttribute("pn", pn);
        return "redirect:/dynamic_table";
    }


    @GetMapping("/dynamic_table")
    public String dynamicTable(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                                Model model){
//        List<User> users = Arrays.asList(new User("zhangsan", "333333"),
//                new User("lisi", "4444444"),
//                new User("wangwu", "555555"),
//                new User("zhaoliu", "666666"));
//        model.addAttribute("users", users);
        // 查询数据库中所有的数据
        List<User> list = userService.list();
//        model.addAttribute("users", list);
        // 分页查询
        Page<User> userPage = new Page<>(pn, 2);
        Page<User> page = userService.page(userPage, null);
        model.addAttribute("page", page);
        return "tables/dynamic_table";
    }

    @GetMapping("/responsive_table")
    public String responsiveTable(){

        return "tables/responsive_table";
    }

    @GetMapping("/pricing_table")
    public String pricingTable(){

        return "tables/pricing_table";
    }

    @GetMapping("/editable_table")
    public String editableTable(){

        return "tables/editable_table";
    }

}

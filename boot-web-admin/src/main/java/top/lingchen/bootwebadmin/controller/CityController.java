package top.lingchen.bootwebadmin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.lingchen.bootwebadmin.bean.City;
import top.lingchen.bootwebadmin.service.CityService;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 10:28
 * @Description
 * @Version 1.0
 */
@Controller
public class CityController {
    @Autowired
    CityService cityService;

    @ResponseBody
    @GetMapping("/city")
    public City getCityById(@RequestParam("id") Integer id){
        return cityService.getCityById(id);
    }

    @ResponseBody
    @PostMapping("/city")
    public City saveCity(City city){
        cityService.saveCity(city);
        return city;
    }
}

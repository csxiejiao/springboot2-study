package top.lingchen.bootwebadmin.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author 凌宸
 * @create 2021-12-07 下午 5:31
 * @Description
 * @Version 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@TableName("user")
public class User {
    /**
     * 所有属性都应该在数据库中存在
     */
    @TableField(exist = false)
    private String username;
    @TableField(exist = false)
    private String password;

    // 以下是数据库需要操作的字段
    private Long id;
    private String name;
    private Integer age;
    private String email;
}

package top.lingchen.bootwebadmin.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 10:26
 * @Description
 * @Version 1.0
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class City {
    private Integer id;
    private String name;
    private String state;
    private String country;
}

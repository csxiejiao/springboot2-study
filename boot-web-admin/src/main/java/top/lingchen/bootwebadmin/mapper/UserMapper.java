package top.lingchen.bootwebadmin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.lingchen.bootwebadmin.bean.User;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 2:29
 * @Description 继承 BaseMapper 类，即可获得 CRUD 功能
 * @Version 1.0
 */
public interface UserMapper extends BaseMapper<User> {

}

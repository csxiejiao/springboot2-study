package top.lingchen.bootwebadmin.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.lingchen.bootwebadmin.bean.Account;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 9:53
 * @Description
 * @Version 1.0
 */
@Mapper
public interface AccountMapper {
    Account getAccountById(Integer id);
}

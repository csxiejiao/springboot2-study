package top.lingchen.bootwebadmin.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import top.lingchen.bootwebadmin.bean.City;

/**
 * @Author 凌宸
 * @create 2021-12-10 下午 10:27
 * @Description
 * @Version 1.0
 */
@Mapper
public interface CityMapper {
    @Select("select * from city where id = #{id}")
    City getCityById(Integer id);

    int insert(City city);
}

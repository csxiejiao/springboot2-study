package top.lingchen.bootwebadmin.actuator.endpoint;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

/**
 * @Author 凌宸
 * @create 2021-12-12 下午 3:02
 * @Description
 * @Version 1.0
 */
@Component
@Endpoint(id = "myService")
public class MyServiceEndpoint {

    @ReadOperation
    public Map getDockerInfo(){
        return Collections.singletonMap("dockerInfo", "dockerStarting...");

    }

    @WriteOperation
    public void stopDocker(){
        System.out.println("docker stopped....");
    }
}

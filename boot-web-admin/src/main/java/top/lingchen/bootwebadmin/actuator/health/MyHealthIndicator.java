package top.lingchen.bootwebadmin.actuator.health;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 凌宸
 * @create 2021-12-12 下午 2:31
 * @Description
 * @Version 1.0
 */
@Component
public class MyHealthIndicator extends AbstractHealthIndicator {
    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        Map<String, Object> map = new HashMap<>();
        if(1 == 1){
            builder.status(Status.UP);
            map.put("count", 10);
            map.put("ms", 100);
        }else{
            builder.status(Status.OUT_OF_SERVICE);
            map.put("error", "连接超时");
            map.put("ms", 3000);
        }
        builder.withDetail("code", 100)
                .withDetails(map);
    }
}

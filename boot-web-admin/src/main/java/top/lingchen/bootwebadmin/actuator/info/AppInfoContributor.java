package top.lingchen.bootwebadmin.actuator.info;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * @Author 凌宸
 * @create 2021-12-12 下午 2:48
 * @Description
 * @Version 1.0
 */
@Component
public class AppInfoContributor implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("msg", "你好")
                .withDetail("hello", "lingchen")
                .withDetails(Collections.singletonMap("world", "164200"));
    }
}

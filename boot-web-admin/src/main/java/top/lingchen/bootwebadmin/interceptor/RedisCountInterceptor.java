package top.lingchen.bootwebadmin.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 4:29
 * @Description
 * @Version 1.0
 */
@Component
public class RedisCountInterceptor implements HandlerInterceptor {
    @Autowired
    StringRedisTemplate redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        // 默认每次访问 uri 就会加 1
        redisTemplate.opsForValue().increment(uri);

        return true;
    }
}

package top.lingchen.bootwebadmin;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.lingchen.bootwebadmin.mapper.UserMapper;

import java.util.concurrent.TimeUnit;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 5:40
 * @Description
 * @Version 1.0
 */

/**
 * @BootstrapWith(SpringBootTestContextBootstrapper.class)
 * @ExtendWith({SpringExtension.class}) 扩展
 */
//@SpringBootTest
@DisplayName("JUnit 5 功能测试类")
public class JUnit5Test {
    @Autowired
    UserMapper userMapper;

    @Test
    @DisplayName("测试 DisplayName 注解")
    void testDisplayName(){
        System.out.println(1);
        System.out.println("userMapper = " + userMapper);
    }

    @Disabled // 禁用
    @Test
    @DisplayName("测试方法2")
    void test2(){
        System.out.println("测试方法2");
    }


    @RepeatedTest(value = 5)
    public void testRepeatedTest(){
        System.out.println("RepeatedTest");
    }

    @Timeout(value = 500, unit = TimeUnit.MILLISECONDS)
    @Test
    void testTimeout() throws InterruptedException {
        Thread.sleep(1000);
    }

    @BeforeEach
    void testBeforeEach(){
        System.out.println("测试就要开始了....");
    }

    @AfterEach
    void testAfterEach(){
        System.out.println("测试结束了....");
    }

    @BeforeAll
    static void testBeforeAll(){
        System.out.println("在所有测试方法之前执行....");
    }

    @AfterAll
    static void testAfterAll(){
        System.out.println("在所有测试方法之后执行....");
    }
}

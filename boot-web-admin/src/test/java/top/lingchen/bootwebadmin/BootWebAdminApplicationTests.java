package top.lingchen.bootwebadmin;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ClusterOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestPropertySource;
import top.lingchen.bootwebadmin.bean.User;
import top.lingchen.bootwebadmin.mapper.UserMapper;

import javax.sql.DataSource;

@Slf4j
@SpringBootTest
@MapperScan("top.lingchen.bootwebadmin.mapper")
class BootWebAdminApplicationTests {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    @Autowired
    UserMapper userMapper;

    @Autowired
    StringRedisTemplate redisTemplate;


    @Test
    void contextLoads() {
        Long count = jdbcTemplate.queryForObject("select count(*) from account_tbl", Long.class);
        log.info("查询得到的记录数为：" + count);
        log.info("数据源的类型" + dataSource.getClass());
    }

    @Test
    public void testUserMapper(){
        User user = userMapper.selectById(1L);
        log.info("用户信息{}" , user);
        System.out.println(user);
    }

    @Test
    public void testRedis(){
        ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
        opsForValue.set("hello", "redis");
        System.out.println(opsForValue.get("hello"));

    }

}

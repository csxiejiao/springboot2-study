package top.lingchen.bootwebadmin;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 6:18
 * @Description
 * @Version 1.0
 */
public class AssertionsTest {

    /**
     * 前面断言失败，后序代码不会执行
     */
    @Test
    @DisplayName("测试简单断言")
    void testSimpleAssertions(){
        int cal = cal(2, 3);
        // 相等
        assertEquals(5, cal, "业务逻辑计算错误");
        // 相同
//        String s1 = new String("hello"); 不通过，以为你对象不同
        String s1 = "hello";
        String s2 = "hello";
        assertSame(s1, s2);
    }

    int cal(int a, int b){
        return a + b;
    }

    @Test
    @DisplayName("测试数组断言")
    public void array(){
        assertArrayEquals(new int[]{1,2}, new int[]{1,2}); // 通过
        assertArrayEquals(new int[]{2,1}, new int[]{1,2}); // 不通过
        // array contents differ at index [0], expected: <2> but was: <1>
    }

    @Test
    @DisplayName("组合断言") // 全部成功才成功，一个失败就是失败
    void testAll(){
        assertAll("test", () -> assertTrue(true && true),
                () -> assertEquals(1, 2)); // expected: <1> but was: <2>
    }

    @Test
    @DisplayName("异常断言")
    void testException(){
        // 断定业务逻辑一定出现异常
        assertThrows(ArithmeticException.class,
                () -> {int i = 10 / 0 ;}, "业务逻辑竟然成功了");

    }

    @Test
    @DisplayName("快速失败")
    void testFail(){
        if(2 == 2){
            fail("测试快速失败");

        }
    }

    /**
     * 测试前置条件
     */
    @DisplayName("测试前置条件")
    @Test
    public void testAssumptions(){
        Assumptions.assumeTrue(false, "结不是 true");
        System.out.println(1111122);
        // org.opentest4j.TestAbortedException: Assumption failed: 结不是 tru
    }
}

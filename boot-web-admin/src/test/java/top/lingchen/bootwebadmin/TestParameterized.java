package top.lingchen.bootwebadmin;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

/**
 * @Author 凌宸
 * @create 2021-12-11 下午 9:53
 * @Description 参数化测试
 * @Version 1.0
 */

public class TestParameterized {

    @DisplayName("参数化测试")
    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5})
    void testValueSource(int i){
        System.out.println("i = " + i);
        /**
         * i = 1
         * i = 2
         * i = 3
         * i = 4
         * i = 5
         */
    }

    @DisplayName("参数化测试数据来源于方法")
    @ParameterizedTest
    @MethodSource("stringStream")
    void testMethodSource(String s){
        System.out.println("s = " + s);
        /**
         * s = apple
         * s = banana
         * s = car
         * s = dog
         */
    }

    static Stream<String> stringStream(){
        return Stream.of("apple","banana", "car", "dog");
    }
}

package top.lingchen.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import top.lingchen.boot.bean.Car;

/**
 * @Author 凌宸
 * @create 2021-12-02 下午 11:44
 * @Description
 * @Version 1.0
 */

@RestController
public class HelloController {

    @Autowired
    private Car car;

    @RequestMapping("/car")
    public Car getCar(){
        return car;
    }

    @RequestMapping("/hello")
    public String hello(){
        return "Hello, Spring Boot 2.x !";
    }
}

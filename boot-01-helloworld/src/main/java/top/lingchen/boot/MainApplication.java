package top.lingchen.boot;

import ch.qos.logback.core.db.DBHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import top.lingchen.boot.bean.Pet;
import top.lingchen.boot.bean.User;
import top.lingchen.boot.config.MyConfig;

/**
 * @Author 凌宸
 * @create 2021-12-02 下午 11:42
 * @Description
 * @Version 1.0
 */
@SpringBootApplication // 这是一个 SpringBoot 应用
//@SpringBootConfiguration
//@EnableAutoConfiguration
//@ComponentScan("top.lingchen.boot")
public class MainApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(MainApplication.class, args);
        // 返回IOC容器

        // 查看容器里所有的组件
        String[] names = run.getBeanDefinitionNames();
        for (String name : names) {
            System.out.println(name);
        }
/**
        // 从容器中获取组件
        User user01 = run.getBean("user01", User.class);
        Pet tom = run.getBean("tom", Pet.class);
        Pet tom1 = run.getBean("tom", Pet.class);

        System.out.println("验证单实例：" + (tom == tom1)); // 验证单实例：true

        // 配置类也是一个组件
        MyConfig bean = run.getBean(MyConfig.class);
        System.out.println(bean);
        // top.lingchen.boot.config.MyConfig$$EnhancerBySpringCGLIB$$e79766fe@d0ec63
        // @Configuration(proxyBeanMethods = true) 验证组件依赖
        System.out.println("组件依赖：" + (user01.getPet() == tom)); // 组件依赖：true

        // 查看 import 导入的组件
        DBHelper bean1 = run.getBean(DBHelper.class);
        System.out.println(bean1); // ch.qos.logback.core.db.DBHelper@41a6d121
         String[] beanNamesForType = run.getBeanNamesForType(User.class);
         for (String name: beanNamesForType) {
         System.out.println(name);
         }
         /**
         * top.lingchen.boot.bean.User
         * user01
         */

        boolean tom = run.containsBean("tom");
        System.out.println("容器中存在 tom 组件：" + tom);
        boolean user01 = run.containsBean("user01");
        System.out.println("容器中存在 user01 组件：" + user01);
        boolean tom2 = run.containsBean("tom2");
        System.out.println("容器中存在 tom2 组件：" + tom2);
        /** @ConditionalOnBean(name = "tom") 加在 public User user01() 方法上
         *  容器中存在 tom 组件：false
         *  容器中存在 user01 组件：false
         *  容器中存在 tom2 组件：true
         */

        /** @ConditionalOnMissingBean(name = "tom") 加在 MyConfig 类上
         * 容器中存在 tom 组件：false
         * 容器中存在 user01 组件：true
         * 容器中存在 tom2 组件：true
         */

        boolean haha = run.containsBean("haha");
        System.out.println(haha); // true
        boolean hehe = run.containsBean("hehe");
        System.out.println(hehe); // true
        User haha1 = run.getBean("haha", User.class);
        System.out.println(haha1); // User{name='lisi', age=22, pet=Pet{name='tomcat2'}}


    }
}

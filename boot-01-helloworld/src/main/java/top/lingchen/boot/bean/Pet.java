package top.lingchen.boot.bean;

/**
 * @Author 凌宸
 * @create 2021-12-03 下午 4:09
 * @Description
 * @Version 1.0
 */
public class Pet {
    private String name;

    public Pet() {
    }

    public Pet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "name='" + name + '\'' +
                '}';
    }
}

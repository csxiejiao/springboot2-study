package top.lingchen.boot.bean;

import lombok.Data;
import lombok.ToString;

/**
 * @Author 凌宸
 * @create 2021-12-03 下午 10:12
 * @Description
 * @Version 1.0
 */
@Data
@ToString
public class Pet {
    private String name;
    private Double weight;
}

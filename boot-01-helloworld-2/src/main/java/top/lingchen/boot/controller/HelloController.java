package top.lingchen.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.lingchen.boot.bean.Person;

/**
 * @Author 凌宸
 * @create 2021-12-03 下午 10:25
 * @Description
 * @Version 1.0
 */
@RestController
public class HelloController {
    @Autowired
    private Person person;

    @RequestMapping("/person")
    public Person person(){
        System.out.println(person.getUserName());
        return person;
    }
}

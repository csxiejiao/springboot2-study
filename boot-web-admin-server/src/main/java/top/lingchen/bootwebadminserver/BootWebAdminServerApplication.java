package top.lingchen.bootwebadminserver;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAdminServer
public class BootWebAdminServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootWebAdminServerApplication.class, args);
    }

}

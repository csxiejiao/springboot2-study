package top.lingchen.bootweb.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * @Author 凌宸
 * @create 2021-12-05 下午 4:41
 * @Description
 * @Version 1.0
 */
@Data
public class Person {
    private String userName;
    private Integer age;
    private Date birth;
    private Pet pet;
}

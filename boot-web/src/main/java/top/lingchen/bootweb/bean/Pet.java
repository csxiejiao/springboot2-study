package top.lingchen.bootweb.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author 凌宸
 * @create 2021-12-05 下午 4:42
 * @Description
 * @Version 1.0
 */
@Data
public class Pet {
    private String name;
    private Integer age;
}

package top.lingchen.bootweb.controller;

import org.springframework.web.bind.annotation.*;

/**
 * @Author 凌宸
 * @create 2021-12-04 下午 8:18
 * @Description
 * @Version 1.0
 */
@RestController
public class HelloController {

//    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @GetMapping("/user")
    public String getUser(){
        return "GET-张三";
    }

//    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @PostMapping("/user")
    public String postUser(){
        return "POST-张三";
    }

//    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    @PutMapping("/user")
    public String putUser(){
        return "PUT-张三";
    }

//    @RequestMapping(value = "/user", method = RequestMethod.DELETE)
    @DeleteMapping("/user")
    public String deleteUser(){
        return "DELETE-张三";
    }


}

package top.lingchen.bootweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author 凌宸
 * @create 2021-12-06 下午 10:02
 * @Description
 * @Version 1.0
 */
@Controller
public class ViewTestController {

    @GetMapping("/lingchen")
    public String lingchen(Model model){
        model.addAttribute("msg", "你好！凌宸！");
        model.addAttribute("link", "www.lingchen1642.com");
        return "success";
    }
}

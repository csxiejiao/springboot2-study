package top.lingchen.bootweb.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import top.lingchen.bootweb.bean.Person;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 凌宸
 * @create 2021-12-04 下午 9:42
 * @Description
 * @Version 1.0
 */
@RestController
public class ParameterTestController {

    @GetMapping("car/{id}/owner/{username}")
    public Map<String, Object> getCar(@PathVariable("id") String id,
                                      @PathVariable("username") String name,
                                      @PathVariable Map<String, String> mp,
                                      @RequestHeader("User-Agent") String userAgent,
                                      @RequestHeader Map<String, String> header,
                                      @RequestParam("age") Integer age,
                                      @RequestParam("inters") List<String> inters,
                                      @RequestParam Map<String, String> params){
        Map<String, Object> map = new HashMap<>();
//        map.put("id", id);
//        map.put("name", name);
//        map.put("mp", mp);
//        map.put("User-Agent", userAgent);
//        map.put("header", header);
        map.put("age", age);
        map.put("inters", inters);
        map.put("params", params);
        return map;
    }

    @PostMapping("/save")
    public Map postMethod(@RequestBody String content){
        Map<String, Object> map = new HashMap<>();
        map.put("content", content);
        return map;
    }

    /**
     * 语法：/cars/sell;low=34;brand=byd,audi,yd
     * SpringBoot默认是禁用了矩阵变量的功能
     *      手动开启，原理，对于路径的处理，UrlPathHelper 进行解析，
     *      将 removeSemicolonContent属性设置为 false
     * 矩阵变量必须要有 url 路径变量才能被解析
     */
    @GetMapping("/cars/{path}")
    public Map carsSell(@MatrixVariable("low") String low,
                        @MatrixVariable("brand") List<String> brand,
                        @PathVariable("path") String path){
        Map<String, Object> map = new HashMap<>();
        map.put("low", low);
        map.put("brand", brand);
        map.put("path", path);
        System.out.println(map); // {path=sell, low=34, brand=[byd, audi, yd]}
        return map;
    }

    @GetMapping("/boss/{bossId}/{empId}")
    public Map boss(@MatrixVariable(value = "age", pathVar = "bossId") Integer bossAge,
                    @MatrixVariable(value = "age", pathVar = "empId") Integer empAge){
        Map<String, Object> map = new HashMap<>();
        map.put("bossAge", bossAge);
        map.put("empAge", empAge);
        System.out.println(map); // {bossAge=20, empAge=10}
        return map;
    }

    /**
     * 数据绑定，页面提交的请求数据 (GET, POST) 都可以和对象属性进行绑定
     * @param person
     * @return
     */
    @PostMapping("/saveUser")
    public Person saveUser(Person person){
        return person;
    }

}

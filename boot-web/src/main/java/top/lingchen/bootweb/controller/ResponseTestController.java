package top.lingchen.bootweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.lingchen.bootweb.bean.Person;

import java.util.Date;

/**
 * @Author 凌宸
 * @create 2021-12-05 下午 8:32
 * @Description
 * @Version 1.0
 */
@Controller
public class ResponseTestController {

    /**
     * 浏览器发送直接返回 xml，[application/xml]   jacksonXmlConverter
     * 如果是 ajav 请求，返回 json，[application/json]   jacksonJsonConverter
     * 如果凌宸 app 发请求返回自定义协议数据，[application/x-lc]   xxxConverter
     *
     * 步骤：
     *  添加自定义的 MessageConverter 进系统底层
     *  系统底层就会统计出所有 MessageConverter 能操作那些类型
     *  客户端内容进行协商 [lc ---> lc]
     * @return
     */
    @ResponseBody
    @GetMapping("/test/person")
    public Person  getPerson(){
        Person person = new Person();
        person.setAge(21);
        person.setBirth(new Date());
        person.setUserName("zhangsan");
        return person;
    }
}
